import NodeCache from 'node-cache';

class CacheService {

  cache: NodeCache

  constructor(ttlSeconds: number) {
    this.cache = new NodeCache({ stdTTL: ttlSeconds, checkperiod: ttlSeconds * 0.2, useClones: false })
  }

  async get(key: string, storeFunction: { (): Promise<any[]> }) {
    const value = this.cache.get(key)
    if (value) {
      return Promise.resolve(value)
    }

    const result = await storeFunction();
    this.cache.set(key, result);
    return result;
  }

  del(key: string) {
    this.cache.del(key)
  }

  flush() {
    this.cache.flushAll()
  }

  stats() : NodeCache.Stats {
    return this.cache.getStats()
  }
}

export default CacheService