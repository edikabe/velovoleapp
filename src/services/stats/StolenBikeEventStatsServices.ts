import StolenBikeEventModel from "../../models/StolenBikeEventModel"
import CacheService from "../CacheService"
import {map, mapObjIndexed, groupBy, pipe, range, sortBy, propEq, find, prop } from "ramda"

const cache = new CacheService(60 * 10) // 10 min cache

interface ByMonth {
  month: number,
  year: number,
  count?: number
}

interface ByMonthAggregationResult {
  _id: ByMonth,
  count: number
}

interface AggregationResult {
  [key: string]: ByMonth[]
}

const defaultMonth = (n: number) => ({ month: n, count: 0 })

const sortByMonth = sortBy((entry) => entry.month)

const mapYear = (yearEntries) => {
  let consolidatedMonths = range(1, 13).map(idx => {
    let monthData: ByMonth = find(propEq("month", idx))(yearEntries)
    return (monthData !== undefined) ? ({ month: monthData.month, count: monthData.count }) : defaultMonth(idx)
  })
  return sortByMonth(consolidatedMonths)
}

const consolidateResults = pipe (//<ByMonthAggregationResult[], ByMonth[], ByMonth[], AggregationResult>(
  map(entry => ({ year: entry._id.year, month: entry._id.month, count: entry.count })),
  groupBy(entry => entry.year as unknown as string),
  mapObjIndexed((months) => {
    let consolidatedMonths = range(1, 13).map(idx => {
      let monthData = find(propEq("month", idx))(months)
      return (monthData !== undefined) ? ({ month: monthData.month, count: monthData.count }) : defaultMonth(idx)
    })
    return sortByMonth(consolidatedMonths)
  })
)

export const aggregateByMonth = async function () {
  const key = "stolenBikesByYear"

  return cache.get(key, async () => {
    let aggregationResults: ByMonthAggregationResult[] = await StolenBikeEventModel.aggregate([
      {
        $project: {
          month: { $month: "$event.date" },
          year: { $year: "$event.date" }
        }
      },
      {
        $group: {
          _id: {
            "month": "$month",
            "year": "$year",
            "groupDate": "$date"
          },
          count: { $sum: 1 }
        }
      }
    ]).exec()

    return consolidateResults(aggregationResults)

  })

}
