import dotenv from 'dotenv'
import { config, S3 } from 'aws-sdk'

// load .env config
dotenv.config()

const {
  S3_HOST,
  S3_KEY_ID,
  S3_SECRET_ACCESS_KEY,
  S3_BUCKET_NAME,
  S3_BUCKET_REGION
} = process.env

config.update({
  accessKeyId: S3_KEY_ID,
  secretAccessKey: S3_SECRET_ACCESS_KEY
})

if(S3_BUCKET_REGION) {
  config.update({region: S3_BUCKET_REGION})
}

// Create S3 service object
export const s3 = new S3({
  endpoint: S3_HOST
  /*,  apiVersion: '2006-03-01'*/
})

export const env = {
  bucket: S3_BUCKET_NAME
}