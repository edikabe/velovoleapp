import mongoose, { Mongoose } from 'mongoose'
import dotenv from 'dotenv'

dotenv.config()

const mongooseConfig = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  connectTimeoutMS: 5000,
  socketTimeoutMS: 10000
}

const {
  DB_HOST,
  DB_NAME,
  DB_USER,
  DB_PASS
} = process.env

async function dbConnect(): Promise<Mongoose> {
  // check if we have a connection to the database or if it's currently
  // connecting or disconnecting (readyState 1, 2 and 3)
  if (mongoose.connection.readyState >= 1) {
    return
  }

  const mongoUri = `mongodb+srv://${DB_USER}:${DB_PASS}@${DB_HOST}/${DB_NAME}?retryWrites=true&w=majority`
  return mongoose.connect(mongoUri, mongooseConfig)
}

export default dbConnect
