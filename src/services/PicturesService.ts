import { s3, env } from "./providers/aws"
import fs from "fs"
import PictureUploadSessionModel, { PictureUploadSession } from "../models/PictureUploadSessionModel"
import type { ReadStream } from "node:fs"
import type { ManagedUpload } from "aws-sdk/clients/s3"


interface S3UploadParams {
  Bucket: string,
  Key: string,
  Body: ReadStream,
  ACL: string
}

interface UploadedPictureData {
  uri: string,
  originalname: string,
  storageType: string
}

const PicturesService = {

  /**
   * Upload pictures on S3.
   * @param {*} param0 
   */
  async uploadPictureOnS3(path: string, originalname: string, filename: string): Promise<UploadedPictureData> {

    // Configure the file stream and obtain the upload parameters
    let fileStream = fs.createReadStream(path)
    fileStream.on('error', function (err) {
      console.log('File Error', err)
    })

    let uploadParams: S3UploadParams = {
      Bucket: `${env.bucket}`,
      Key: `bikepictures/${filename}/${originalname}`,
      ACL: 'public-read',
      Body: fileStream
    }

    return new Promise((resolve, reject) => {
      s3.upload(uploadParams, function (err: Error, data: ManagedUpload.SendData) {
        if (err) {
          reject(err)
        }
        if (data) {
          resolve({
            uri: data.Location,
            originalname,
            storageType: 'S3-bucket'
          })
        }
      })
    })
  },

  async saveTempPicture(pictureFile, session: string) {
    return new PictureUploadSessionModel({
      filename: pictureFile.filename,
      mimetype: pictureFile.mimetype,
      originalname: pictureFile.originalname,
      path: pictureFile.path,
      upload: new Date(),
      session: session
    }).save()
      .catch(err => console.log(err))
  },

  /**
   * Get temp uploaded pictures by session.
   */
  async getTempUploadedPicturesBySession(session: string): Promise<Array<PictureUploadSession>> {
    return PictureUploadSessionModel.find({ "session": session }).exec()
  },

  async deleteAllTempUploadedPicturesBySession(session: string) {
    PictureUploadSessionModel.deleteMany({ session: session }).exec()
    let pictures: PictureUploadSession[] = await this.getTempUploadedPicturesBySession(session)
    pictures.forEach(picture => fs.unlink(picture.path, (err) => { if (err) console.error(err) }))
    return "ok"
  }

}

export default PicturesService