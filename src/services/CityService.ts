import StolenBikeEventModel from '../models/StolenBikeEventModel'
import CacheService from './CacheService'

const cache = new CacheService(60 * 60 * 1) // 60minutes cache

export class CityService2 {
  private cacheInstance: CacheService
  constructor() {
    this.cacheInstance = new CacheService(60 * 60 * 1)
  }
  async getAllCities() {
    const cities = await this.cacheInstance.get('cities', async () => {
      return StolenBikeEventModel.distinct("event.location.city")
    })
    return cities    
  }
}


const CityService = {

  async getAllCities() {
    const cities = await cache.get('cities', async () => {
      return StolenBikeEventModel.distinct("event.location.city").exec()
    })
    return cities
    
  },
  plop(): string {
    return "plop"
  }
  
}

export default CityService