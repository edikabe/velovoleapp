import nodemailer from 'nodemailer'
import Mustache from 'mustache'
import stolen_bike_email from '../templates/email/stolen_bike_email.mustache'
import dotenv from "dotenv"

dotenv.config()

const {
  MAIL_SMTP_USER,
  MAIL_SMTP_PASS,
  MAIL_SMTP_HOST,
  MAIL_SMTP_PORT,
  APP_EMAIL_SENDER_NAME,
  APP_EMAIL_SENDER_EMAIL
} = process.env

const transporter = nodemailer.createTransport(
  `smtps://${MAIL_SMTP_USER}:${MAIL_SMTP_PASS}@${MAIL_SMTP_HOST}`
) 

const EmailService = {
  async sendThankYourAlertEmail(data) {
    return transporter.sendMail({
      from:  `"${APP_EMAIL_SENDER_NAME}" <${APP_EMAIL_SENDER_EMAIL}>`, // sender address
      to: data.user.email, 
      subject: "Merci de votre signalement 🚴‍♂️", 
      html: Mustache.render(stolen_bike_email, data) // html body
    })
  }
}

export default EmailService