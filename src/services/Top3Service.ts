import CacheService from './CacheService'
import StolenBikeEventModel from '../models/StolenBikeEventModel'

const cache = new CacheService(60 * 1) // 60secs TTL cache

const Top3Service = {

  getCacheStats() {
    return cache.stats()
  },

  async getTop3NeighbourhoodByCity(city: string) {

    const key = `getTop3NeighbourhoodByCity_${city}`

    return cache.get(key, async () => {
      let result = await StolenBikeEventModel.aggregate([
        {
          $match: { "event.location.city": city }
        },
        {
          $group: {
            _id: "$event.location.neighbourhood",
            count: { $sum: 1 }
          }
        },
        {
          $sort: { count: -1 }
        },
        {
          $limit: 3
        }
      ]).exec()

      return result

    }).then((result) => {
      return result
    })

  }

}

export default Top3Service