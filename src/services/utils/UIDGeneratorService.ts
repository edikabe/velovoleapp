import UIDGenerator from "uid-generator"

const uidGen128 = new UIDGenerator()
const uidGen256 = new UIDGenerator(256)
const uidGen512 = new UIDGenerator(512)

const UIDGeneratorService = {
  async generate(): Promise<string> {
    return uidGen128.generate() 
  },
  async generate256(): Promise<string> {
    return uidGen256.generate() 
  },
  async generate512(): Promise<string> {
    return uidGen512.generate() 
  }
}

export default UIDGeneratorService