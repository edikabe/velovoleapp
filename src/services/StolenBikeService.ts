import StolenBikeEventModel, { StolenBikeEvent, projections } from "../models/StolenBikeEventModel"
import type { LeanDocument } from "mongoose"

interface Pagination {
  pages: number,
  current: number
}

interface PaginatedResults {
  data: LeanDocument<StolenBikeEvent[]>,
  pagination: Pagination
}

const StolenBikeService = {

  async getAll(): Promise<Array<StolenBikeEvent>> {
    return StolenBikeEventModel.find()
      .sort({ "event.date": -1 })
      .exec()
  },

  async getValidated(): Promise<Array<StolenBikeEvent>> {
    return StolenBikeEventModel.find({ "admin.hasBeenValidated": true })
      .sort({ "event.date": -1 })
      .exec()
  },

  async getPaginated(limit: number = 5, page: number = 1): Promise<PaginatedResults> {
    const [documents, total] = await Promise.all([
      StolenBikeEventModel.find({ "admin.hasBeenValidated": true }, projections.PUBLIC).limit(limit).skip((page - 1) * limit).sort({ "event.date": -1 }).lean().exec(),
      StolenBikeEventModel.count({ "admin.hasBeenValidated": true }).exec()
    ])
    const pageCount = Math.ceil(total / limit)

    return {
      data: documents,
      pagination: {
        pages: pageCount,
        current: page
      }
    }
  }

}

export default StolenBikeService