import mongoose from 'mongoose'
import EventSchema, { BikeEvent } from './schemas/EventSchema'
import '../services/providers/db'

export interface StolenBikeEvent extends BikeEvent { }

export const projections = {
  PUBLIC: { "event": 1, "bike.brand": 1, "bike.model": 1, "bike.bikeType": 1, "bike.colors": 1, "bike.pictures": 1, "user.name": 1, "createdAt": 1 }
}

export default mongoose.models['stolen_bikes'] || mongoose.model<StolenBikeEvent>('stolen_bikes', EventSchema)
