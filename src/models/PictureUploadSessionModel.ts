import mongoose, { Schema, Document, Model } from 'mongoose'
import '../services/providers/db'

export interface PictureUploadSession extends Document {
  filename: string,
  mimetype: string,
  originalname: string,
  path: string,
  session: string,
  upload: Date 
}

export const PictureUploadSessionSchema = new Schema({
  filename: String,
  mimetype: String,
  originalname: String,
  path: String,
  session: {type: String, index: true},
  upload: {type: Date, index: true} 
})

const model: Model<PictureUploadSession> = mongoose.model<PictureUploadSession>('picture_upload_session', PictureUploadSessionSchema)

export default model
