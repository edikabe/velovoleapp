import mongoose from 'mongoose'
import EventSchema from './schemas/EventSchema'
import '../services/providers/db'

const FoundBikeEvent = mongoose.model('found_bikes', EventSchema)
export default FoundBikeEvent