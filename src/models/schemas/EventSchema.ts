import { Schema, Document } from 'mongoose'

import {
  REQUIRED_STRING_TYPE,
  REQUIRED_NUMBER_TYPE,
  REQUIRED_DATE_TYPE
} from './shared-types'


export interface LatLng {
  lat: number,
  lng: number
}

/**
* Shared EVENT schema for stolen and found bikes.
*/
const LatLngSchema = new Schema({
  "lat": REQUIRED_NUMBER_TYPE,
  "lng": REQUIRED_NUMBER_TYPE
}, { _id: false })

export interface Location {
  latlng: LatLng,
  city: string,
  neighbourhood: string
}

const LocationSchema = new Schema({
  latlng: {
    type: LatLngSchema,
    required: true
  },
  city: REQUIRED_STRING_TYPE,
  neighbourhood: REQUIRED_STRING_TYPE
}, { _id: false })

export interface Event {
  date: Date,
  location: {
    type: Location,
    required: true
  }
}

export interface Picture {
  path: string,
  originalname: string,
  uri: string,
  storageType: string
}

const PictureSchema = new Schema({
  //path: REQUIRED_STRING_TYPE,
  originalname: REQUIRED_STRING_TYPE,
  uri: REQUIRED_STRING_TYPE,
  storageType: REQUIRED_STRING_TYPE
}, { _id: false })

export interface Color {
  name: string,
  hex: string
}

const ColorSchema = new Schema({
  name: REQUIRED_STRING_TYPE,
  hex: REQUIRED_STRING_TYPE
}, { _id: false })

export interface Bike {
  brand: string,
  model: string,
  serialNumber: string,
  bikeType: string,
  details: string,
  pictures: Array<Picture>,
  colors: Array<Color>
}

export interface User {
  name: string,
  email: string
}

export interface Misc {
  howDidYouKnowAboutUs: string
}

export interface AdminPart {
  hasBeenValidated: boolean
}

export interface BikeEvent extends Document {
  createdAt: Date,
  admin: AdminPart,
  event: Event,
  bike: Bike,
  user: User,
  misc: Misc
}

export const EventSchema = new Schema({
  createdAt: REQUIRED_DATE_TYPE,
  admin: {
    hasBeenValidated: {
      type: Boolean,
      required: true
    }
  },
  event: {
    date: Date,
    location: {
      type: LocationSchema,
      required: true
    }
  },
  bike: {
    brand: REQUIRED_STRING_TYPE,
    model: String,
    serialNumber: String,
    bikeType: REQUIRED_STRING_TYPE,
    details: String,
    pictures: [{
      type: PictureSchema
    }],
    colors: [{
      type: ColorSchema
    }]
  },
  user: {
    name: REQUIRED_STRING_TYPE,
    email: String
  },
  misc: {
    howDidYouKnowAboutUs: REQUIRED_STRING_TYPE
  }
})

// indexes
EventSchema.index({ "event.location.city": 1 })
EventSchema.index({ "event.date": -1 })
EventSchema.index({ brand: 1, model: 1 })
EventSchema.index({ "admin.hasBeenValidated": 1 })

export default EventSchema