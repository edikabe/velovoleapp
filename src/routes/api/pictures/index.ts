import send from '@polka/send-type'
import { compose } from "compose-middleware"
import multer from 'multer'
import PicturesService from "../../../services/PicturesService"
import UIDGeneratorService from "../../../services/utils/UIDGeneratorService"

const upload = multer({ dest: "/tmp/velovoleapp/pictures/" }).any()

const handlePost = async function(req, res, next) {

  const session = req.headers['x-form-session'] as string
  const pictureFile = req.files[0]

  PicturesService.saveTempPicture(pictureFile, session)
    .then(async () => {
      res.end(await UIDGeneratorService.generate()) 
    })
    .catch((err) => send(res, 500, {details: err}))
  
}

export const post = compose([upload, handlePost])

export function del(req, res, next) {
  send(res, 501)
}