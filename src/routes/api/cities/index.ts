import send from '@polka/send-type';
import type { Request, Response } from "express"
import CityService from '../../../services/CityService'

export const get = async (req: Request, res: Response) => CityService.getAllCities()
  .then(data => {
    send(res, 200, data)
  })