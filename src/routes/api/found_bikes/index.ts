import send from '@polka/send-type';
import FoundBikeEventModel from '../../../models/FoundBikeEventModel'


export async function get(req, res, next) {
  res.setHeader('Content-Type', 'application/json')
  FoundBikeEventModel.find()
    .then(docs => {
      send(res, 200, {"found_bikes": docs})
    })
    .catch(() => {
      send(res, 500, {"message": "Something went wrong"})
    })
}

export async function post(req, res, next) {

  console.log(req.body)

  new FoundBikeEventModel(req.body)
    .save()
    .then((doc) => {
      console.log(arguments)
      res.setHeader('Content-Type', 'application/json')
      send(res, 200, {"message": "ok"})
    })
    .catch((err) => {
      console.log(err)
      res.setHeader('Content-Type', 'application/json')
      send(res, 500, {"message": "something went wrong"})
    })

}