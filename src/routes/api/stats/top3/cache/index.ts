import send from "@polka/send-type"
import type { Request, Response } from "express"
import Top3Service from "../../../../../services/Top3Service"

export async function get(req: Request, res: Response) {

  const stats = Top3Service.getCacheStats()
  send(res, 200, stats)

}