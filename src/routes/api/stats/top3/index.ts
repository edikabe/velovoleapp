import send from "@polka/send-type"
import type { Request, Response } from "express"
import Top3Service from "../../../../services/Top3Service"

export async function get(req: Request, res: Response) {

  Top3Service.getTop3NeighbourhoodByCity(req.query.city as string)
    .then(data => {
      send(res, 200, data)
    })

}