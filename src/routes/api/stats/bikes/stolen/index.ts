import send from "@polka/send-type"
import { aggregateByMonth } from "../../../../../services/stats/StolenBikeEventStatsServices"

export const get = async function(req, res) {
    const data = await aggregateByMonth()
    send(res, 200, {data: data})
}