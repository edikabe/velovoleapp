
import UIDGeneratorService from "../../../../services/utils/UIDGeneratorService"
import send from "@polka/send-type"
import type { Request, Response } from "express"

async function generateFromRequestParam(req: Request): Promise<string> {
  const bytes = req.query.b
  if(bytes) {
    if(bytes === "128") {
      return UIDGeneratorService.generate() 
    } else if (bytes === "256") {
      return UIDGeneratorService.generate256() 
    } else if (bytes === "512") {
      return UIDGeneratorService.generate512() 
    } else {
      return UIDGeneratorService.generate()
    }
  } else {
    return UIDGeneratorService.generate()
  }
}

export async function post(req: Request, res: Response) {
  const uid = await generateFromRequestParam(req)
  send(res, 200, { uid })
}