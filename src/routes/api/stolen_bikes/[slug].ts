import send from '@polka/send-type';
import StolenBikeEventModel, { projections } from '../../../models/StolenBikeEventModel'

export async function get(req, res, next) {
  const { slug } = req.params;
  StolenBikeEventModel.find({ _id: slug }, projections.PUBLIC)
    .then(docs => {
      const event = docs[0]
      send(res, 200, { "stolen_bike": event })
    })
    .catch(() => {
      send(res, 500, { "message": "Something went wrong" })
    })
}