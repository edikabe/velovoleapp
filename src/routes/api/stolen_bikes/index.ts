import send from "@polka/send-type"
import type { Request, Response } from "express"
import { getQueryParamAsNumber } from "../../../lib/express-helpers"
import StolenBikeEventModel from "../../../models/StolenBikeEventModel"
import StolenBikeService from "../../../services/StolenBikeService"
import PicturesService from "../../../services/PicturesService"
import EmailService from "../../../services/EmailService"
// import UIDGeneratorService from "../../../services/utils/UIDGeneratorService"

/**
 * List all stolen bikes.
 */
export function get(req: Request, res: Response) {

  const limit = getQueryParamAsNumber("limit", req, 10, true)
  const page = getQueryParamAsNumber("page", req, 1, true)

  StolenBikeService.getPaginated(limit, page)
    .then(result => {
      send(res, 200, result)
    })
    .catch((err) => {
      console.error(err)
      send(res, 500, { "message": "Something went wrong", "details": err })
    })
}

/**
 * Create a new stolen_bike event.
 */
export async function post(req: Request, res: Response) {

  const session = req.headers['x-form-session'] as string

  const pictureUploadSessions = await PicturesService.getTempUploadedPicturesBySession(session)

  let uploadS3Results = await Promise.all(pictureUploadSessions.map(picData => PicturesService.uploadPictureOnS3(picData.path, picData.originalname, picData.filename)))

  const formData = req.body
  // on merge les resultats de l'upload S3
  formData.bike.pictures = uploadS3Results

  //TODO refacto en service 

  const event = new StolenBikeEventModel(formData)
  // event.set('_id', await UIDGeneratorService.generate256())
  event.set('createdAt', new Date())
  event.set("admin.hasBeenValidated", false)
  const errors = event.validateSync()

  if (errors) {
    send(res, 400, {
      "message": "Invalid request",
      "errors": errors
    })
    return
  }

  event
    .save()
    .then((doc) => {

      Promise.all([
        PicturesService.deleteAllTempUploadedPicturesBySession(session),
        EmailService.sendThankYourAlertEmail(doc)
      ]).catch(err => console.log(err))

      res.setHeader('Content-Type', 'application/json')
      send(res, 201, {
        "message": "ok",
        "_id": doc._id
      })
    })
    .catch((err) => {
      // console.log(err)
      res.setHeader('Content-Type', 'application/json')
      send(res, 500, {
        "message": "something went wrong"
      })
    })

}