import type { Request } from "express"

export const getQueryParamAsNumber = function (param: string, req: Request, defaultValue: number, shouldBeStrictlyPositive: boolean = false): number {
  if (!req.query[param]) {
    return defaultValue
  }
  let paramValue: string = req.query[param] as string
  try {
    let parsedValue = parseInt(paramValue)
    if(shouldBeStrictlyPositive && parsedValue <= 0) {
      return defaultValue
    }
    return parsedValue
  } catch (err) { console.debug(err) }
  return defaultValue
}