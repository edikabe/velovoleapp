import sirv from 'sirv';
import polka from 'polka';
import { json } from 'body-parser';
import compression from 'compression';
import * as sapper from '@sapper/server';
import dotenv from "dotenv"
import dbConnect from "../src/services/providers/db"

dotenv.config()

const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === 'development';

dbConnect()
	.then(() => {
		polka() // You can also use Express
			.use(json())
			.use(
				compression({ threshold: 0 }),
				sirv('static', { dev }),
				sapper.middleware({
					session: () => ({
						config: {
							appApiUrl: process.env.APP_API_URL
						}
					})
				})
			)
			.listen(PORT, err => {
				if (err) console.log('error', err);
			});
	})

