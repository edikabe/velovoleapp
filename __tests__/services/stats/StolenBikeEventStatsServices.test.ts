import 'jest-extended'
import mockingoose from "mockingoose"
import StolenBikeEventModel from "../../../src/models/StolenBikeEventModel"
import { aggregateByMonth } from "../../../src/services/stats/StolenBikeEventStatsServices"
import R from "ramda"

test("ramda", () => {
  
  // given 
  let input: number[] = [1,2,3]

  // when 
  let result = R.pipe<number[], string[]>(
    R.map(x => `x${x}`)
  )(input)

  let expected = ["x1","x2","x3"]
  // then 
  expect(result).toIncludeAllMembers(expected)
})

test("StolenBikeEventStatsServices#aggregateByMonth()", async () => {

  let expected = {
    "2021": [
      {
        "month": 1,
        "count": 1
      },
      {
        "month": 2,
        "count": 0
      },
      {
        "month": 3,
        "count": 3
      },
      {
        "month": 4,
        "count": 0
      },
      {
        "month": 5,
        "count": 0
      },
      {
        "month": 6,
        "count": 0
      },
      {
        "month": 7,
        "count": 0
      },
      {
        "month": 8,
        "count": 0
      },
      {
        "month": 9,
        "count": 0
      },
      {
        "month": 10,
        "count": 0
      },
      {
        "month": 11,
        "count": 0
      },
      {
        "month": 12,
        "count": 0
      }
    ]
  }

  let mockResult = [
    {
      "_id": {
          "month": 1,
          "year": 2021,
          "groupDate": "2021-01-01T00:00:00.000Z"
      },
      "count": 1
    },
    {
      "_id": {
          "month": 3,
          "year": 2021,
          "groupDate": "2021-03-01T00:00:00.000Z"
      },
      "count": 3
    }
  ]

  mockingoose(StolenBikeEventModel).toReturn(mockResult, "aggregate")

  return aggregateByMonth().then(result => {
    Object.keys(result).forEach(key => {
      expect(result[key]).toIncludeAllMembers(expected[key])
    })    
  })

})
