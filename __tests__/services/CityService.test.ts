import mockingoose from 'mockingoose'
import CityService, { CityService2 } from "../../src/services/CityService"
import StolenBikeEventModel from "../../src/models/StolenBikeEventModel"

test('getAllCities', async () => {
  mockingoose(StolenBikeEventModel).toReturn(["Nantes", "Vertou"], "distinct");
  return CityService.getAllCities().then((result) => {
    expect(result).toEqual(["Nantes", "Vertou"])
  })
})

test('CityService2#getAllCities()', async () => {
  mockingoose(StolenBikeEventModel).toReturn(["Nantes", "Vertou"], "distinct");
  return new CityService2().getAllCities().then((result) => {
    expect(result).toEqual(["Nantes", "Vertou"])
  })
})